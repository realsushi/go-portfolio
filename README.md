# Portfolio website
[site](https://realsushi.gitlab.io/go-portfolio)
[Hugo](https://gohugo.io) website using [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ce/user/project/pages/).

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install `git` and `go`.
1. [Install](https://gohugo.io/getting-started/installing/) Hugo.
	1. `sudo pacman -S go hugo`
1. install or load modules
	1. load the theme as a Hugo module:

   ```shell
   hugo mod init realsushi.gitlab.io/go-portfolio
   hugo mod get github.com/CaiJimmy/hugo-theme-stack/v3
   ```

	1. install theme for deeper modification and commit it
  ```s
  cd themes
  git clone --depth=1  github.com/CaiJimmy/hugo-theme-stack/
  ```	
1. Preview your project:

   ```shell
   hugo server -D
   ```
   **http://localhost:1313/go-portfolio/**

1. Add content > remove draft flag.
1. Optional. Generate the website:

   ```shell
   hugo
   ```
1. accelerate build with Gitlab-CI
	git is downloaded everytime and stored in cache for x versions.
	- remove copies/versions from cli or add depth parameter
	- debloat history with bfg (wiping commit history is not cleaning)
	- remove any binary stuff from the repo

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

## Use a custom theme

Hugo supports a variety of themes.

Visit <https://themes.gohugo.io/> and pick the theme you want to use. In the
Pages example, we use <https://themes.gohugo.io/themes/gohugo-theme-ananke/>.


## `hugo` vs `hugo_extended`

The [Container Registry](https://gitlab.com/pages/hugo/container_registry)
contains two kinds of Hugo Docker images, `hugo` and
`hugo_extended`. Their main difference is that `hugo_extended` comes with
Sass/SCSS support. If you don't know if your theme supports it, it's safe to
use `hugo_extended` since it's a superset of `hugo`.

The Container Registry contains three repositories:

- `registry.gitlab.com/pages/hugo`
- `registry.gitlab.com/pages/hugo/hugo`
- `registry.gitlab.com/pages/hugo/hugo_extended`

`pages/hugo:<version>` and `pages/hugo/hugo:<version>` are effectively the same.
`hugo_extended` was created afterwards, so we had to create the `pages/hugo/` namespace.

See [how the images are built and deployed](https://gitlab.com/pages/hugo/-/blob/707b8e367cdea5dbf471ff5bbec9f684ae51de79/.gitlab-ci.yml#L36-47).

## GitLab User or Group Pages

To use this project as your user/group website, you will need to perform
some additional steps:

1. Rename your project to `namespace.gitlab.io`, where `namespace` is
   your `username` or `groupname`. This can be done by navigating to your
   project's **Settings > General (Advanced)**.
1. Change the `baseurl` setting in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
   Proceed equally if you are using a custom domain: `baseurl = "https://example.com"`.

Read more about [GitLab Pages for projects and user/group websites](https://docs.gitlab.com/ce/user/project/pages/getting_started_part_one.html).


## Troubleshooting

## Content creation
### adding hugo shortcodes/modules
[shortcode insert image for example](https://gohugo.io/content-management/shortcodes/)

### dealing with images

#### local content images
- add image to post folder like cover.jpg
- rename article to index.md instead of hello-world.md

#### global path for images
- add image to `static`
*risk of content orphaning/splatering*

#### **script to resize image**
avoid commit to unbloat history
> [imagemagick](https://imagemagick.org/script/index.php) (need to update [crew](https://github.com/chromebrew/chromebrew/issues?q=is%3Aissue+libpng) in case of dependancy not found)

```s
$ tree -LQh 3
.
├── [4.0K]  "autocoin"
│   ├── [427K]  "Screenshot 2022-01-21 at 15.40.14.png"
│   ├── [873K]  "Screenshot 2022-03-09 at 15.34.57.png"
│   ├── [204K]  "Screenshot_20220311_121617.png"
│   ├── [603K]  "Screenshot 2022-03-15 at 16.20.15.png"
│   ├── [1.2M]  "Screenshot 2022-03-16 at 15.26.14.png"
│   ├── [211K]  "Screenshot 2022-03-25 at 16.25.41.png"
│   └── [833K]  "Screenshot 2022-03-28 at 10.49.36.png"
├── [4.0K]  "data-analysis-app"
│   └── [347K]  "Screenshot 2022-02-04 at 10.36.44.png"
├── [4.0K]  "graph-network"
│   ├── [161K]  "Screenshot 2022-01-25 at 22.13.32.png"
│   ├── [750K]  "Screenshot 2022-01-25 at 23.42.16.png"
│   ├── [954K]  "Screenshot 2022-01-26 at 10.28.45.png"
│   └── [258K]  "Screenshot 2022-01-28 at 22.51.23.png"
└── [4.0K]  "nrj-app"
    ├── [1.9M]  "Screenshot 2022-01-14 at 15.26.59.png"
    ├── [546K]  "Screenshot 2022-03-28 at 10.46.51.png"
    └── [1.5M]  "Screenshot 2022-06-13 at 11.40.17.png"
```

use compression algortithms using
`mogrify *.png -quality 10`
see [doc](https://imagemagick.org/script/mogrify.php)


```s
.
├── [4.0K]  "autocoin"
│   ├── [255K]  "Screenshot 2022-01-21 at 15.40.14.png"
│   ├── [727K]  "Screenshot 2022-03-09 at 15.34.57.png"
│   ├── [200K]  "Screenshot_20220311_121617.png"
│   ├── [498K]  "Screenshot 2022-03-15 at 16.20.15.png"
│   ├── [494K]  "Screenshot 2022-03-16 at 15.26.14.png"
│   ├── [142K]  "Screenshot 2022-03-25 at 16.25.41.png"
│   └── [344K]  "Screenshot 2022-03-28 at 10.49.36.png"
├── [4.0K]  "data-analysis-app"
│   └── [233K]  "Screenshot 2022-02-04 at 10.36.44.png"
├── [4.0K]  "graph-network"
│   ├── [136K]  "Screenshot 2022-01-25 at 22.13.32.png"
│   ├── [397K]  "Screenshot 2022-01-25 at 23.42.16.png"
│   ├── [885K]  "Screenshot 2022-01-26 at 10.28.45.png"
│   └── [182K]  "Screenshot 2022-01-28 at 22.51.23.png"
└── [4.0K]  "nrj-app"
    ├── [1.0M]  "Screenshot 2022-01-14 at 15.26.59.png"
    ├── [277K]  "Screenshot 2022-03-28 at 10.46.51.png"
    └── [1.3M]  "Screenshot 2022-06-13 at 11.40.17.png"
```


## Optimization
### CICD for builds
- [ ] todo later
keep the dev build private while exposing the binary builds
### image hosting
- [ ] todo
can host on photos.google using API

