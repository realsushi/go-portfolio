---
title: Links
links:
  - title: GitLab
    description: My GitLab porfile and repositories
    website: https://gitlab.com/realsushi
    image: https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png
  
  - title: LinkedIn Profile
    description: My linedin profile to see my career
    website: https://linkedin.com/in/etienne-leroy-dev42
    image: ts-logo-128.jpg

  - title: Hobby blog
    description: A blog about gardening
    website: https://walipili.com
    image: ts-logo-128.jpg
menu:
    main: 
        weight: 4
        params:
            icon: link

comments: false

#`image` field accepts both local and external images.
---

