---
title: "Todo List"
description: 
slug: todo-list
date: 2022-08-13T00:10:02+02:00
image: cover.jpg 
math: 
license: 
hidden: false
comments: true
draft: false
---

# Project list

- 2009
    - MS Excel VBA
    - Catia V5 r18 VBA scripts
- 2010
    - Linux scripting
- 2011
    - Camel stuff
- 2012
    - Fortran stuff
    - Octave stuff
    - Fortran stuff
    - Linux testing on atom cpu
    - computer assembly stuff
- 2013
    - optimized stuff
    - classic mechanical stuff
- 2015
    - Linux daily driver stuff
- 2016
    - blog
    - website
    - git workflows
- 2017
    - js automation on Adobe suite
- 2018
    - Catia V5 r21 generative designe high level automation
- 2019
    - IT stuff
    - matlab stuff aerospace
    - infrastructure stuff
    - go hugo blog
    - C system calls
    - aquaponic 1

- 2020
    - python programing 2D algorithm for games
    - aquaponic 2
    - networking and automation
    - C++ programing*
    - ecommerce mobile app
    - restful api app
    - computer vision 1
    - angular touch friendly app
    - microservice apps with Docker
    - automated app and server with Ancible
    - scalable apps with Kubernetes
    - virtual gaming buddy app
    - agile complex app design
    - system engineering app design top down
    - change management workflows
    - java app

- 2021
    - aquaponic IOT sensors device
    - crypto trading app 1

- 2022
    - crypto trading app2

- 2023
