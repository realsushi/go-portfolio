---
title: "Energy Optimization App"
description: 
date: 2022-08-22T19:09:17+02:00
image: "cover-nrj-app.png"
math: 
license: 
hidden: false
comments: true
draft: false
categories:
  - python
  - java
  - iot
---

# Reduce Carbon footprint with CO2 consumption app in Python

## Usecase

A client wants to review and possibly reduce energy consumption for a manufactoring line up to a whole plant possibly.

By plugging some sensors and monitoring the data it was possible to identify solutions and projects possible return over investment with our solution.

## Exploring data through a dashboard
First version is to recover data and push it through an influx-db unstructured database (btw influx 1.8 legacy has sql like syntax for QL). Java mail client > MQTT broker >  Parsing and database consolidation. This workflow has been refactored multiple times, java, python, low-code with node-red, the micro-service containerized in python.
{{< figure src="Screenshot 2022-06-13 at 11.40.17.png" title="data monitoring through MQTT broker endpoint - IOT style" >}}

## Frontend is in Python with Dash-Plotly

Frontend was designed on the roots of a Covid-chart application to keep the same CSS and User-Experience. In short to keep things simple. 

## Backend is in Python using FastAPI frontend

While computation could have been done by the client directly it was a bold move to enable an REST-like API. Indeed the project has evolved multiple times and it was possible to just update routes instead of scraping everything.

## Database with sql-lite/postgres
how to structure data to avoid redundancy at the cost of limited development
{{< figure src="Screenshot 2022-03-28 at 10.46.51.png" title="Database UML thinking user-app or data-user and redundancy?" >}}

## UI/UX
UI is based on covid tracker and CSS is from Dash Plotly's Bootstrap implementation
UX has 2 directions Data > Optimization/savings > Business Case/ROI
and then chart > customization with user parameters (incl ROI calculations)

{{< figure src="Screenshot 2022-01-14 at 15.26.59.png" title="UX flows on top of UI placeholders" >}}

## Devops

The whole project has been docker compliant from the start, orchestrated with docker-compose. Kubernetes was not an option because it was only a solid proof of concept app.
Lot of documenation to explain everything using docstring added
At each commit a fex Gitlab-CI jobs would store linting and a few tests
A job would build the docker and add context to be stored on gitlab private registry