---
title: "Biggest Project of 2022"
description:
slug: autocoin 
date: 2022-08-12T23:44:26+02:00
image: "cover-cryptoexchange-app.png"
math: 
license: 
hidden: false
comments: true
draft: false
categories:
  - python
  - blockchain
tags: 
  - 2022
---

# Biggest project of 2022 (and most of 2021)

For the moment, the biggest project of 2022 was into BlockChain and trading with crypto exchanges.

Autocoin was a project about micro transactions trading and day to day trading.

Our client, the user, would use our interface to view the market and perform trade while being assisted.


## Features of the app:
1. frontend written in Python (Dash Plotly)
2. backend Python (FastAPI)
1. Models of Artificial Intelligence built using Tensorflow2/Python
3. small database in sqlite
4. some trading bots


## The plotting friendly UI in Python
After some fiddling with react.js in a competitive solution approach, a Python version was selected. It is using Dash by Plotly framework to integrate faster with charts.

While the User Interface is minimalisticly simple, the User Experience has been consolidated and improved multiple times.

{{< figure src="Screenshot 2022-01-21 at 15.40.14.png" title="figure1: Python Front End" >}}

UX would have 2 flows:
- Horizontally, the user would go deeper into the data - in short API calls to Binance markets
- Vertically the user would go deeper into commands (where the orders should be configured)
The different elements of the app would be structured as functional elements of User-Needs. Indeed UserStories were structured in a pyramis scheme, a basic user would only ead data, while a more experienced client would mostly focus on the more advanced tools.

  Actually, a FrontEnd should not host AI frameworks and models for a reason. It was interesting to see how problems started to appears very quickly after adding some IA workloads on the front.

{{< figure src="Screenshot 2022-03-16 at 15.26.14.png" title="figure4: Backend user management feature User Interfacec" >}}


## block-API feature
{{< figure src="Screenshot 2022-03-28 at 10.49.36.png" title="figure6: API endpoints with FastAPI and ReDoc (docstring)" >}}
This feature had already a version working in Django framework for Python but it was rather undocumented so a choice was made to scrap everything to embrace the nacent popularity of FastAPI.
- be capable to ingest external data aka source-of-thruth for markets
- allow secure user management
- reduce external API polling limitations by aggregating data
- serve AI models to offload the frontend

{{< figure src="Screenshot 2022-03-15 at 16.20.15.png" title="figure3: Backend shady stuff - hash salt pepper" >}}
{{< figure src="Screenshot_20220311_121617.png" title="figure7: Backend user management - single username rule" >}}


## Status update
The frontend of the app has performed its duty of showcasing the functional modules.
The upcoming version would take a stronger foothold within the mobile interface ecosystem by getting developped in mobile first framework.

Also it's rather complex to keep up with a team remotely for 2 years so the team would likely shrink and split up for better competitive results.


## State of the art
The market of crypto investment has evolved a lot during the project.
At the begining it was relatively easy to put orders on non-KYC exchanges it was just before the last crypto boom. However KYC started to become a thing so it was more popular to go with Binance at the end.
Our product could integrate with Kryll.io ecosystem. At the end of the Python version, Kryll would have started to deliver automated features.

{{< figure src="Screenshot 2022-03-09 at 15.34.57.png" title="figure7: upcoming Binance features" >}}


## stall'd features
All the community features were scraped and not delivered/showcased.