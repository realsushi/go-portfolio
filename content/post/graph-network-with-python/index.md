---
title: "Graph Network With Python"
description: 
date: 2022-08-22T19:08:50+02:00
image: "cover-graph-app-grey.png" 
math: 
license: 
hidden: false
comments: true
draft: false
categories:
  - python
---
# Graph network integration in Python websites thanks to Dash Plotly

Dash Plotly is a framework using Flask, so it is very similar to Flask but with the addition of nice modules.
These are undirected graphs

## Usecase - when to really draw connected graphs
- Need to write a technical documentation on a topic (ie, thesis / slideshare)
- Need to sum up ideas in another format than just bullet points
- Need to draw a map
- Need to do a directed graph for computation
- Need to build a knowledge graph for AI, especially for NLP and themes of close-meanings-objects

## Matplotlib - static
A very simple cumbersome graph with 1000 nodes.
Completely static, the code must be modifed at each iteration if the number of nodes increases
{{< figure src="Screenshot 2022-01-25 at 22.13.32.png" title="figure 1: Matplotlib" >}}

## Dot graph - less static but still
Using dot syntax it is rather easy to build a graph from scratch using just text.
The graph is very nice, altough some blobs should be more separated from the others
 {{< figure src="Screenshot 2022-01-25 at 23.42.16.png" title="figure 2: dot graph" >}}

## The interactive way using Dash Plotly versus network-graph lib
The dataset is randomized to serve as placeholder.
Graph can be zoomed in, UX is excellent.
Colors reprensent the weight

{{< figure src="Screenshot 2022-01-26 at 10.28.45.png" title="figure 3: dash plotly" >}}

## Cytonize - Best of both worlds: interactive and easy to add data

The dataset is very simple, one can add/delete node but also modify the representation like previously. And one can manipulate a node.
{{< figure src="Screenshot 2022-01-28 at 22.51.23.png" title="figure 4: Cytonize from Dash Plotly" >}}