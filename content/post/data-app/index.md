---
title: "Data App"
description: 
date: 2022-08-22T19:08:17+02:00
image: "cover-data-app.png"
math: 
license: 
hidden: false
comments: true
draft: false
categories:
  - python
---

# Data app with Python and ELK stack

## The Python part

After receiving a big chunk of unstructured textual data it is good to vizualize things and try to identify some segmentation.

The app is a Dash-Plotly Python frontend with an integration with an ELK stack on docker-compose 